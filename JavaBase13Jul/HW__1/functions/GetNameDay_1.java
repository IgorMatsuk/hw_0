package functions;

/*
 * 1. Получить строковое название дня недели по номеру дня.
 * */
public class GetNameDay_1 {
    public static String getNameDay(int numDay) {
        String name = null;
        switch (numDay) {
            case 1:
                name = "Monday";
                break;
            case 2:
                name = "Tuesday";
                break;
            case 3:
                name = "Wednesday";
                break;
            case 4:
                name = "Thursday";
                break;
            case 5:
                name = "Friday";
                break;
            case 6:
                name = "Saturday";
                break;
            case 7:
                name = "Sunday";
                break;
        }
        return name;
    }

}
