package arrays;

import java.util.Arrays;

/*
 * 8. Поменять местами первую и вторую половину массива, например,
 * для массива 1 2 3 4, результат 3 4 1 2
 * */
public class HalfArrayReverse {

    public static int[] halfReverse(int[] myArray) {
        int temp = 0;
        for (int i = 0; i < myArray.length / 2; i++) {
            temp = myArray[i];
            myArray[i] = myArray[myArray.length/2 + i];
            myArray[myArray.length/2 + i] = temp;
        }
        return myArray;
    }
}

