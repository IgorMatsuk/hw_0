package arrays;

/*
 * 2. Найти максимальный элемент массива
 * */
public class MaxArrayElem {
    public static int maxArrElement(int[] array) {
        int maxElem = array[0];
        for (int i = 0; i < array.length; i++) {
            if (maxElem < array[i]) {
                maxElem = array[i];
            }
        }
        return maxElem;
    }
}