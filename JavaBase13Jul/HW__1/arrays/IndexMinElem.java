package arrays;

/*
 * 3. Найти индекс минимального элемента массива
 * */
public class IndexMinElem {
    public static int indexMinEl(int[] array) {
        int minElement = array[0];
        int indexMinElement = 0;
        for (int i = 0; i < array.length; i++) {
            if (minElement > array[i]) {
                minElement = array[i];
                indexMinElement = i;
            }
        }
        return indexMinElement;
    }
}