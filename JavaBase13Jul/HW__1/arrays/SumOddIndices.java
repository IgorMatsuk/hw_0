package arrays;

import java.util.Arrays;

/*
 * 5. Посчитать сумму элементов массива с нечетными индексами
 * */
public class SumOddIndices {
    public static int sumElemWithOddIndex(int[] array) {
        int sumOddInd = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 != 0) {
                sumOddInd = array[i] + sumOddInd;
            }
        }
        return sumOddInd;
    }
}