package arrays;
/*
 * 4. Найти индекс максимального элемента массива
 * */
public class IndexMaxElem {
    public static int indexMaxEl(int[] array) {
        int maxElement = 0;
        int indexMaxElement = 0;
        for (int i = 0; i < array.length; i++) {
            if (maxElement < array[i]) {
                maxElement = array[i];
                indexMaxElement = i;
            }
        }
        return indexMaxElement;
    }
}

