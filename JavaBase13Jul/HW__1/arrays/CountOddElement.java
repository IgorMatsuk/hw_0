package arrays;

/*
 * 7. Посчитать количество нечетных элементов массива
 * */
public class CountOddElement {

    public static int countElem(int[] myArray) {
        int item = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] % 2 > 0) {
                item++;
            }
        }
        return item;
    }

}
