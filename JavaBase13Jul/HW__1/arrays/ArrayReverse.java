package arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

/*
 * 6. Сделать реверс массива (массив в обратном направлении)
 *
 * */
public class ArrayReverse {

    public static int[] revers(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
        return array;
    }

}
