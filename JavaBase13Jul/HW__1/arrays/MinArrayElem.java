package arrays;

/*
 * 1. Найти минимальный элемент массива
 * */
public class MinArrayElem {
    public static int minArrEl(int[] array) {
        int minElem = array[0];
        for (int i = 0; i < array.length; i++) {
            if (minElem > array[i]) {
                minElem = array[i];
            }
        }
        return minElem;
    }
}