package cycles;

/*
3.Найти корень натурального числа с точностью до целого
(рассмотреть вариант последовательного подбора и метод бинарного поиска)
*/
public class NaturalNumberRoot {

    public static int returnSqrtNaturalNumber(int number) {
        int natSqrt = (int) Math.round(Math.sqrt(number));
        return natSqrt;
    }
}
