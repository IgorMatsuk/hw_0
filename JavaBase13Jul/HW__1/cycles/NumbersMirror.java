package cycles;

/*
6. Вывести число, которое является зеркальным отображением последовательности цифр
 заданного числа, например, задано число 123, вывести 321.
 */
public class NumbersMirror {
    public static int returnMirrorNumber(int number) {
        return number < 10 ? number : Integer.parseInt(String.valueOf(number % 10) + returnMirrorNumber(number / 10));
    }
}
