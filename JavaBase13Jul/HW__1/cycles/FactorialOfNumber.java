package cycles;

/*
 * 4. Вычислить факториал числа n. n! = 1*2*…*n-1*n;!
 */
public class FactorialOfNumber {
    public static int returnFactorialOfNumber(int number) {
        int result = 1;
        for (int i = 1; i <= number; i++) {
            result *= i;
        }
        return result;
    }
}
