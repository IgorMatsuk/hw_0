package cycles;

/*
1. Найти сумму четных чисел и их количество в диапазоне от 1 до 99
*/
public class SumAndAmountEveNumbers {

    public static int returnSumEveNumbers() {
        int sum = 0;
        for (int i = 1; i <= 99; i++) {
            if (i % 2 == 0) {
                sum = sum + i;
            }
        }
        return sum;
    }

    public static int returnAmountEveNumbers() {
        int amount = 0;
        for (int i = 1; i <= 99; i++) {
            if (i % 2 == 0) {
                amount++;
            }
        }
        return amount;
    }
}
