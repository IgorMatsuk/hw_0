package cycles;

/*
 * 5. Посчитать сумму цифр заданного числа
 * */
public class NumberSum {

    public static int sumOfAllNumbers(int number) {
        int result = 0;
        for (int i = 1; i <= number; i++) {
            result += i;
        }
        return result;
    }

}