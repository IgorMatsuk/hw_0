package tests.t_functions;

import functions.GetNameDay_1;
import org.junit.Test;

import static org.junit.Assert.*;

public class GetNameDay_1Test {

    @Test
    public void getNameDay() {
        String result = "Friday";
        assertEquals(GetNameDay_1.getNameDay(5),result);
    }
}