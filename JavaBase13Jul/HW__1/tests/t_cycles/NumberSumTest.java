package tests.t_cycles;

import cycles.NumberSum;
import org.junit.Test;

import static org.junit.Assert.*;

public class NumberSumTest {

    @Test
    public void sumOfAllNumbers() {
        int result = 15;
        assertEquals(NumberSum.sumOfAllNumbers(5),result);
    }
}