package tests.t_cycles;

import cycles.FactorialOfNumber;
import org.junit.Test;

import java.beans.PropertyEditorSupport;

import static org.junit.Assert.*;

public class FactorialOfNumberTest {

    @Test
    public void returnFactorialOfNumber() {
        int result = 6;
        assertEquals(FactorialOfNumber.returnFactorialOfNumber(3),result);
    }
}