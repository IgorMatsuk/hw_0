package tests.t_cycles;

import cycles.SumAndAmountEveNumbers;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumAndAmountEveNumbersTest {

    @Test
    public void returnSumEveNumbers() {
        int resultSum = 2450;
        assertEquals(SumAndAmountEveNumbers.returnSumEveNumbers(),resultSum);

    }
    @Test
    public void returnAmountEveNumbers(){
        int ResultAmount = 49;
        assertEquals(SumAndAmountEveNumbers.returnAmountEveNumbers(),ResultAmount);
    }
}