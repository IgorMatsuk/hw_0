package tests.t_cycles;

import cycles.PrimeNumber;
import org.junit.Assert;
import org.junit.Test;

public class PrimeNumberTest {

    @Test
    public void isThisNumberPrime() {
        boolean resultPrNum = true;
        Assert.assertEquals(PrimeNumber.isThisNumberPrime(29), resultPrNum);
    }
}