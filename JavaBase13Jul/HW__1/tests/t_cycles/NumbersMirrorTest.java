package tests.t_cycles;

import cycles.NumbersMirror;
import org.junit.Test;

import static org.junit.Assert.*;

public class NumbersMirrorTest {

    @Test
    public void returnMirrorNumber() {
        int result = 12345;
        assertEquals(NumbersMirror.returnMirrorNumber(54321), result);
    }
}