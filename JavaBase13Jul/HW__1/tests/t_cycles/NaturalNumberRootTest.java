package tests.t_cycles;

import cycles.NaturalNumberRoot;
import org.junit.Test;

import static org.junit.Assert.*;

public class NaturalNumberRootTest {

    @Test
    public void returnSqrtNaturalNumber() {
        int result =7;
        assertEquals(NaturalNumberRoot.returnSqrtNaturalNumber(56),result);
    }
}