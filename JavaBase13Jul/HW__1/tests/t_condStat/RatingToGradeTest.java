package tests.t_condStat;

import condStat.RatingToGrade;
import org.junit.Test;

import static org.junit.Assert.*;

public class RatingToGradeTest {

    @Test
    public void convertRatingToGrade() {
        String result = "B";
        assertEquals(RatingToGrade.convertRatingToGrade(89),result);
    }
}