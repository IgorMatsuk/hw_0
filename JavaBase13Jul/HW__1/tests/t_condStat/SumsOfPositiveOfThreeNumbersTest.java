package tests.t_condStat;

import condStat.SumsOfPositiveOfThreeNumbers;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumsOfPositiveOfThreeNumbersTest {

    @Test
    public void sumsPositiveNumbers() {
        int result = 14;
        assertEquals(SumsOfPositiveOfThreeNumbers.sumsPositiveNumbers(12, 2, -3), result);
    }
}