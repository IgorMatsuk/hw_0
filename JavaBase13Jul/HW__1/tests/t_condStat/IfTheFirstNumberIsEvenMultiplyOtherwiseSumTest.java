package tests.t_condStat;

import condStat.IfTheFirstNumberIsEvenMultiplyOtherwiseSum;
import org.junit.Test;

import static org.junit.Assert.*;

public class IfTheFirstNumberIsEvenMultiplyOtherwiseSumTest {
    int evenNumber = 2;
    int oddNumber = 3;
    int b = 5;

    @Test
    public void sumOddNumbers() {
        int result = 8;
        assertEquals(IfTheFirstNumberIsEvenMultiplyOtherwiseSum.sumOddNumbers(oddNumber,b),result);
    }

    @Test
    public void multiplyOddNumbers() {
        int result = 10;
        assertEquals(IfTheFirstNumberIsEvenMultiplyOtherwiseSum.sumOddNumbers(evenNumber,b),result);
    }
}