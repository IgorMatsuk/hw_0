package tests.t_condStat;

import condStat.DefineQuarterPointWithCoordinates;
import org.junit.Test;

import static org.junit.Assert.*;

//2.Определить какой четверти принадлежит точка с координатами (х,у)
public class DefineQuarterPointWithCoordinatesTest {

    @Test
    public void defineQuarterPointFirst() {
        int result = 1;
        assertEquals(DefineQuarterPointWithCoordinates.defineQuarterPoint(23, 56), result);
    }

    @Test
    public void defineQuarterPointSecond() {
        int result = 2;
        assertEquals(DefineQuarterPointWithCoordinates.defineQuarterPoint(-23, 56), result);
    }

    @Test
    public void defineQuarterPointThird() {
        int result = 3;
        assertEquals(DefineQuarterPointWithCoordinates.defineQuarterPoint(-23, -56), result);
    }

    @Test
    public void defineQuarterPointFourth() {
        int result = 4;
        assertEquals(DefineQuarterPointWithCoordinates.defineQuarterPoint(23, -56), result);
    }
}