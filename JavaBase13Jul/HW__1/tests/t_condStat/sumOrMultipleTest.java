package tests.t_condStat;

import condStat.SumOrMultiple;
import org.junit.Test;

import static org.junit.Assert.*;

public class sumOrMultipleTest {

    @Test
    public void returnSumExpression() {
        int resultSum = 8;
        assertEquals(SumOrMultiple.returnSumOrMultiple(1, 2, 2), resultSum);

    }

    @Test
    public void returnMultipleExpression() {
        int resultMultiple = 15;
        assertEquals(SumOrMultiple.returnSumOrMultiple(3, 2, 2), resultMultiple);

    }
}