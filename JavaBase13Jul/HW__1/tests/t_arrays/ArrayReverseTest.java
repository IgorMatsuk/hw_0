package tests.t_arrays;

import arrays.ArrayReverse;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayReverseTest {

    @Test
    public void reversArray() {
     int [] array = {10,9,8,7,6,5,4,3,2};
     int [] result = {2,3,4,5,6,7,8,9,10};
     assertArrayEquals(ArrayReverse.revers(array),result);
    }
}