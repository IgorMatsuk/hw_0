package tests.t_arrays;

import arrays.IndexMaxElem;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndexMaxElemTest {

    @Test
    public void indexMaxEl() {
        int [] array = {12,23,54,76,78,12,34,56};
        int result = 4;
        assertEquals(IndexMaxElem.indexMaxEl(array),result);
    }
}