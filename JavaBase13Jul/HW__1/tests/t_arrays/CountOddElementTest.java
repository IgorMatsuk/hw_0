package tests.t_arrays;

import arrays.CountOddElement;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountOddElementTest {

    @Test
    public void countElem() {
        int[] array = {1, 3, 5, 6, 8, 9, 1};
        int result = 5;
        assertEquals(CountOddElement.countElem(array), result);
    }


}