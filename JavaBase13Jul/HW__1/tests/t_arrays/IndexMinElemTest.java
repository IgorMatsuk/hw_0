package tests.t_arrays;

import arrays.IndexMinElem;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndexMinElemTest {

    @Test
    public void indexMinEl() {
        int[] array = {12, 23, 6, 7, 2, 12, 56};
        int result = 4;
        assertEquals(IndexMinElem.indexMinEl(array), result);
    }
}