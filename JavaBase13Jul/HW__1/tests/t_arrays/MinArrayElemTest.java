package tests.t_arrays;

import arrays.MinArrayElem;
import org.junit.Test;

import static org.junit.Assert.*;

public class MinArrayElemTest {

    @Test
    public void minArrEl() {
        int[] array = {12, 5, 67, 34, 2, 67, -98};
        int result = -98;
        assertEquals(MinArrayElem.minArrEl(array), result);
    }
}