package tests.t_arrays;

import arrays.MaxArrayElem;
import org.junit.Test;

import static org.junit.Assert.*;

public class MaxArrayElemTest {

    @Test
    public void maxArrElement() {
        int [] array = {12,9,78,23,90,34};
        int result = 90;
        assertEquals(MaxArrayElem.maxArrElement(array),result);
    }
}