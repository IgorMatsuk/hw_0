package tests.t_arrays;

import arrays.SumOddIndices;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumOddIndicesTest {

    @Test
    public void sumElemWithOddIndex() {
        int [] array = {12,30,4,30,45,30,23,10};
        int result = 100;
        assertEquals(SumOddIndices.sumElemWithOddIndex(array),result);
    }
}