package tests.t_arrays;

import arrays.SortArray;
import org.junit.Test;

import static org.junit.Assert.*;

public class SortArrayTest {
    int[] array = {12, 56, 3, 89, 1, 3, 6, 78, 2, 1};
    int[] result = {1, 1, 2, 3, 3, 6, 12, 56, 78, 89};

    @Test
    public void bubbleSort() {
        assertArrayEquals(SortArray.bubbleSort(array), result);
    }

    @Test
    public void selectSort() {
        assertArrayEquals(SortArray.insertionSort(array), result);
    }

    @Test
    public void insertionSort() {
        assertArrayEquals(SortArray.selectSort(array), result);
    }
}