package tests.t_arrays;

import arrays.HalfArrayReverse;
import org.junit.Test;

import static org.junit.Assert.*;

public class HalfArrayReverseTest {

    @Test
    public void halfReverse() {
        int [] array = {1,2,3,4};
        int [] result = {3,4,1,2};
        assertArrayEquals(HalfArrayReverse.halfReverse(array),result);
    }
}