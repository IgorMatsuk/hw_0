package condStat;
/*
1. Если а – четное посчитать а*б, иначе а+б
*/

public class IfTheFirstNumberIsEvenMultiplyOtherwiseSum {
    public static int sumOddNumbers(int a, int b) {
        if (a % 2 != 0) {
            return a + b;
        } else return a * b;
    }
}