package condStat;

import java.io.Serializable;

/*
5. Написать программу определения оценки студента по его рейтингу, на основе следующих правил
Рейтинг  Оценка
0-19   --- F
20-39  --- E
40-59  --- D
60-74  --- C
75-89  --- B
90-100 --- A
*/
public class RatingToGrade {

    public static String convertRatingToGrade(int a) {
        if (a >= 0 && a < 20)
            return ("F");
        if (a >= 20 && a < 40)
            return ("E");
        if (a >= 40 && a < 60)
            return ("D");
        if (a >= 60 && a < 75)
            return ("C");
        if (a >= 75 && a < 90)
            return ("B");
        if (a >= 90 && a < 100)
            return ("A");
        return "Something went wrong";
    }
}
