package condStat;

/*
3.Найти суммы только положительных из трех чисел
*/
public class SumsOfPositiveOfThreeNumbers {
    public static int sumsPositiveNumbers(int a, int b, int c) {
        if (a > 0 && b > 0 && c > 0)
            return (a + b + c);
        if (a > 0 && b > 0 && c < 0)
            return (a + b);
        if (a > 0 && b < 0 && c > 0)
            return (a + c);
        if (a < 0 && b > 0 && c > 0)
            return (b + c);
        return 0;
    }

}
