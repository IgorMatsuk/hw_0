package condStat;

/*
2.Определить какой четверти принадлежит точка с координатами (х,у)
*/
public class DefineQuarterPointWithCoordinates {
    public static int defineQuarterPoint(int x, int y) {
        if (x > 0 && y > 0)
            return 1;
        if (x < 0 && y > 0)
            return 2;
        if (x < 0 && y < 0)
            return 3;
        if (x > 0 && y < 0)
            return 4;
        return 0;
    }
}
