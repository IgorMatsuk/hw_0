package condStat;

/*
Посчитать выражение макс(а*б*с, а+б+с)+3
*/
public class SumOrMultiple {
    public static int returnSumOrMultiple(int a, int b, int c) {
        if (((a * b * c) + 3) > ((a + b + c) + 3)) {
            return ((a * b * c) + 3);
        } else
            return ((a + b + c) + 3);
    }
}
