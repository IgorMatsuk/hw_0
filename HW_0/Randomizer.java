package HW_0;


public class Randomizer {

    public static void main(String[] args) {

        System.out.print("1. Вывести на консоль случайное число: \n");
        task1();
        System.out.print("\n2. Вывести на консоль 10 случайных чисел: \n");
        task2();
        System.out.print("\n3. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10: \n");
        task3();
        System.out.println("\n4. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50: ");
        task4();
        System.out.println("\n5.Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10: ");
        task5();
        System.out.println("\n6.Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных\n" +
                " чисел, каждое в диапазоне от -10 до 35: \n");
        task6();

    }

    //1. Вывести на консоль случайное число.
    public static void task1() {
        System.out.println((Math.random()));
    }

    //2. Вывести на консоль 10 случайных чисел.
    public static void task2() {
        for (int i = 0; i < 10; i++) {
            double random = (Math.random());
            System.out.println(random);
        }
    }

    //3. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.
    public static void task3() {
        for (int i = 0; i < 10; i++) {
            int random = (int) (Math.random() * 10 + 1);
            System.out.println(random);
        }
    }

    //4. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.
    public static void task4() {
        for (int i = 0; i < 10; i++) {
            int max = 50;
            int min = 20;
            max -= min;
            System.out.println((int) (Math.random() * ++max + min));
        }
    }

    //5.Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.
    public static void task5() {
        for (int i = 0; i < 10; i++) {
            int max = 10;
            int min = -10;
            max -= min;
            System.out.println((int) (Math.random() * ++max + (--min)));
        }
    }

    //6.Вывести на консоль случайное количество (в диапазоне от 3 до 15)
    //случайных чисел, каждое в диапазоне от -10 до 35.
    public static void task6() {
        int maxRange = 15;
        int minRange = 3;
        maxRange -= minRange;
        int randomNum = (int) (Math.random() * ++maxRange + (--minRange));
        System.out.println("Количество случайных чисел: " + randomNum);
        for (int i = 0; i < randomNum; i++) {
            int max = 35;
            int min = -10;
            max -= min;
            System.out.println((int) (Math.random() * ++max + (--min)));
        }
    }
}